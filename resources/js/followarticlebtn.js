$( document ).ready(function() {

    $(document).ready(function() {
        $("#follow-button").click(function(e) {
            e.stopPropagation();
            $("#follow-btn-container").toggle();
        });

        $(document).click(function(e) {
            if (!$(e.target).is("#follow-button") && !$(e.target).is("#follow-btn-container") && $("#follow-btn-container").is(":visible")) {
                $("#follow-btn-container").hide();
            }
        });

        $("#follow-btn-container").click(function(e) {
            e.stopPropagation();
        });
    });

});

$('.follow-btn-radio').on('click',function(){
    var state = $(this).val();
    
    var payload_json = {
		action: 'watch',
        titles:RLCONF.wgPageName,
        format:'json',
	}

    if(state==0){
        payload_json = Object.assign({}, payload_json, {unwatch: 1});
    }

    api = new mw.Api();
    api.postWithToken( 'watch', payload_json ).done( function ( data ) {
        console.log(data);
        if(data.watch[0].unwatched!=undefined){
            $('#follow-button').html('Follow');
        }
        if(data.watch[0].watched!=undefined){
            $('#follow-button').html('Unfollow');
        }
        $("body").click();        
    } ).fail(function (data){
        console.log(data);
        $("body").click();
    }); 

})