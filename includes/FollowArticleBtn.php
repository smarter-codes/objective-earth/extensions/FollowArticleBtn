<?php

use MediaWiki\MediaWikiServices;

/**
 * Upvote class 
*/
 
class FollowArticleBtn {

	public static function onParserFirstCallInit( $parser ) {
		
	}	

	/**
	 * Adds the two new required database tables into the database when the
	 * end-user (sysadmin) runs /maintenance/update.php
	 * (the core database updater script) and performs other DB updates, such as
	 * the renaming of tables, if upgrading from an older version of this extension.
	 *
	 * @param DatabaseUpdater $updater
	 */
	public static function onLoadExtensionSchemaUpdates( $updater ) {
        

    }

	public static function onBeforePageDisplay( OutputPage $out, Skin $skin ){
		$out->addModules( [ 'ext.followarticlebtn' ] );
	}


	public static function onArticleViewHeader( &$article, &$outputDone, &$pcache ) {

		global $wgUser, $wgServer, $wgDBserver, $wgDBuser, $wgDBpassword, $wgDBname, $wgScriptPath, $wgRequest, $wgOut;
		
		if($wgUser->isAnon()){
			return ;
		}

		$conn = mysqli_connect("$wgDBserver","$wgDBuser","$wgDBpassword","$wgDBname");

		$title = Title::newFromText($wgRequest->getVal('title'));
		$pageNameWithUnderScores = $title->getDBkey();

		$userId = $wgUser->getId();

		$sql = "SELECT * FROM `watchlist` WHERE `wl_title`='$pageNameWithUnderScores' AND `wl_user`='$userId'";
		$result = mysqli_query($conn, $sql); 
		$total = mysqli_num_rows($result);

		$followed_check = "";
		$unfollowed_check = "";
		$follow_text = "";

		if($total>0){
			$followed_check="checked";
			$follow_text = "Unfollow";
		}else{
			$unfollowed_check="checked";
			$follow_text = "Follow";
		}

		$title = Title::newFromText( $wgOut->getPageTitle() );
		$categories = $title->getParentCategories();
		
		$category_keys = [];
		foreach($categories as $key=>$value){
			$category_keys[] = $key;
		}
		
		$selected_cats = ['Category:Problem','Category:Solution','Category:Project','Category:Region'];

		// check if current category is in the list of values in db
		$check_arr = array_intersect($category_keys, $selected_cats);

		if(count($check_arr)<=0){
			return ;
		}
		

		$article->getContext()->getOutput()->addHTML("
			<div id='follow-btn-main-container'>
				<button id='follow-button'>$follow_text</button>
				<div id='follow-btn-container'>
					<div>
					<div class='follow-input-form'>
						<input class='follow-btn-radio' type='radio' id='follow' value='1' name='follow' $followed_check>
						<div class='input-label' style='margin-left: 10px;'>
							<label for='follow'>Following</label>
							<br/>
							<small>You will be notified of this page's activity</small>
						</div>
					</div>
					<div class='follow-input-form'>
						<input class='follow-btn-radio' type='radio' id='unfollow' value='0' name='follow' $unfollowed_check>
						<div class='input-label'>
							<label for='unfollow'>Unfollow</label>
							<br/>
							<small>You won't be notified of this page's activity</small>
						</div>
					</div>
					</div>
				</div>
			</div>
		");
	}

}